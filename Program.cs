using CrananiumApi.Data.Config;
using CrananiumApi.Data.Repository;
using CrananiumApi.Domain.Datasource;
using CrananiumApi.Domain.Games;
using CrananiumApi.Domain.Service;
using CrananiumApi.Domain.Usecase;
using CrananiumApi.Domain.UseCase;
using CrananiumApi.Presentation.Hubs;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using CrananiumApi.Data.Config.Games;
using CrananiumApi.Data.MongoRepository.Config;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Text.Json.Serialization;
using System.Text.Json;
using CrananiumApi.Domain.Model;

namespace CrananiumApi
{
    public class Program
    {
        public class LowerCase : JsonNamingPolicy
        {
            public override string ConvertName(string name)
            {
                return name.ToLower();
            }
        }
        public static void Main(string[] args)
        {
            /*
             * MongoDB
             * name: CrananiumDB
             * password: Epkr8DaD1WZQ3WiY
             * mongodb+srv://CrananiumDB:<password>@maincluster.tvsdhkf.mongodb.net/?retryWrites=true&w=majority
             */
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddControllers();
            //    .AddNewtonsoftJson(options =>
            //{
            //    options.SerializerSettings.Converters.Add(new StringEnumConverter());
            //});

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            // Datasources
            builder.Services.AddScoped<MongoDatabase>();
            builder.Services.AddScoped<UserDatasource, UserMongoRepository>();
            builder.Services.AddScoped<RoomDatasource, RoomMongoRepository>();
            builder.Services.AddScoped<GameDatasource, GameMongoRepository>();

            // Games
            builder.Services.AddScoped<IGameSettings, SongGuessChosen>();
            builder.Services.AddScoped<IGameSettings, SongGuessRandom>();
            builder.Services.AddScoped<IGameSettings, TriviaRandom>();
            builder.Services.AddScoped<GameFactory>();

            // Services
            builder.Services.AddScoped<RoomService, RoomUseCase>();
            //builder.Services.AddScoped<UserService, UserUseCase>();
            builder.Services.AddScoped<GameService, GameUseCase>();

            // Handlers
            builder.Services.AddScoped<RoomHandler>();
            builder.Services.AddScoped<IGameHandler, QuickGameHandler>();
            builder.Services.AddScoped<IGameHandler, SongGuessGameHandler>();



            // Configuration
            builder.Services.Configure<AppConfig>(
                builder.Configuration.GetSection("App"));
            builder.Services.Configure<DatabaseConfig>(
                builder.Configuration.GetSection("CrananiumCoreDatabase"));


            var pack = new ConventionPack { new CamelCaseElementNameConvention() };
            ConventionRegistry.Register(nameof(CamelCaseElementNameConvention), pack, _ => true);


            builder.Services.AddSignalR();

            builder.Services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins("*")
                            .AllowAnyHeader()
                            .WithMethods("GET", "POST", "PUT");
                            //.AllowCredentials();
                    });
            });

            var port = Environment.GetEnvironmentVariable("PORT") ?? "8080";
            var url = $"http://127.0.0.1:{port}";
            builder.WebHost.UseUrls(url);

            var app = builder.Build();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseSwagger();
            //if (app.Environment.IsDevelopment())
            //{
            app.UseSwaggerUI();
            //}

            app.UseHttpsRedirection();

            app.UseAuthorization();
            app.UseCors();

            app.MapControllers();

            app.MapHub<RoomHub>("/hub");
            app.MapHub<MessagesHub>("/messages");

            app.Run();
        }
    }
}