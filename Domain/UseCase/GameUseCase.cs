﻿using CrananiumApi.Data.Config;
using CrananiumApi.Domain.Datasource;
using CrananiumApi.Domain.Games;
using CrananiumApi.Domain.Model;
using CrananiumApi.Domain.Service;

namespace CrananiumApi.Domain.UseCase
{
    public class GameUseCase : GameService
    {
        private readonly GameFactory _gameFactory;
        private readonly GameDatasource _datasource;

        public GameUseCase(GameFactory gameFactory, GameDatasource gameDatasource)
        {
            _datasource = gameDatasource;
            _gameFactory = gameFactory;
        }

        public async Task<Game> GenerateNextGame(Room room)
        {
            var driverId = room.GetNextDriverId();
            var nameIndex = Random.Shared.Next(0, room.Config.Games.Count);
            var gameName = room.Config.Games[nameIndex];

            var gameSettings = _gameFactory.From(gameName);

            var game = new Game(gameSettings.Props, Const.Modes.ALL, driverId)
            {
                Data = gameSettings.Data
            };

            return await _datasource.CreateAsync(game);
        }
    }
}
