﻿using CrananiumApi.Data.Config;
using CrananiumApi.Domain.Datasource;
using CrananiumApi.Domain.Model;
using CrananiumApi.Domain.Service;
using Microsoft.Extensions.Options;
using static CrananiumApi.Data.Config.Const;

namespace CrananiumApi.Domain.Usecase
{
    public class RoomUseCase : RoomService
    {
        private readonly IOptions<AppConfig> _appConfig;
        private readonly GameService _gameService;
        private readonly RoomDatasource _roomDatasource;
        private readonly UserDatasource _userDatasource;
        private readonly GameDatasource _gameDatasource;

        public RoomUseCase(IOptions<AppConfig> appConfig, RoomDatasource datasource, UserDatasource userDatasource, GameDatasource gameDatasource, GameService gameService)
        {
            _appConfig = appConfig;
            _roomDatasource = datasource;
            _userDatasource = userDatasource;
            _gameDatasource = gameDatasource;
            _gameService = gameService;
        }

        public async Task<Room> CreateRoom(string username, GameConfig? config)
        {
            var user = await _userDatasource.CreateAsync(username);
            var gameConfig = config ?? _appConfig.Value.DefaultGameConfig;

            var room = new Room(Scene.Lobby, gameConfig)
            {
                Users = new List<User> { user }
            };

            room.Game = await _gameService.GenerateNextGame(room);
            return await _roomDatasource.CreateAsync(room);
        }

        public async Task<Room> GetRoom(string id)
        {
            return await _roomDatasource.GetAsync(id);
        }

        public async Task<Room> UpdateRoom(Room room)
        {
            return await _roomDatasource.UpdateAsync(room);
        }

        public async Task<Room> JoinRoom(string roomId, string username)
        {
            var user = await _userDatasource.CreateAsync(username);
            return await _roomDatasource.AddUserAsync(roomId, user);
        }

        public async Task<Room> UpdateUser(string roomId, User user)
        {
            var updateUser = await _userDatasource.UpdateAsync(user);
            return await _roomDatasource.UpdateUserAsync(roomId, updateUser);
        }

        public async Task<Room> UpdateRoomScene(string roomId, string scene)
        {
            return await _roomDatasource.UpdateSceneAsync(roomId, scene);
        }

        public async Task<Room> AddGame(Room room, Game game)
        {
            var newGame = await _gameDatasource.CreateAsync(game);
            await _roomDatasource.AddGameAsync(room.Id, newGame);
            return await _roomDatasource.GetAsync(room.Id);
        }

        public async Task UpdateGame(string roomId, Game game)
        {
            await _roomDatasource.AddGameAsync(roomId, game);
        }

        public async Task UpdateGameData(string roomId, GameData data)
        {
            await _roomDatasource.SetGameDataAsync(roomId, data);
        }
    }
}
