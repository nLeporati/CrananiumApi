﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json.Converters;
using System.Text.Json.Serialization;

namespace CrananiumApi.Domain.Model
{
    public class Room
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = null!;

        private List<User> _users;

        public List<User> Users
        {
            get { return _users; }
            set
            {
                _users = value;
                AllUserStatus = CheckAllUserStatus();
            }
        }

        public Game? Game { get; set; }

        //[BsonRepresentation(BsonType.String)]
        //[JsonConverter(typeof(StringEnumConverter))]
        public string Scene { get; set; }

        public List<Game> Results { get; set; }

        public GameConfig Config { get; set; }

        public Room(string scene, GameConfig config)
        {
            Scene = scene;
            _users = new();
            Results = new();
            AllUserStatus = CheckAllUserStatus();
            Config = config;
        }


        /* Custom functions */

        [BsonIgnore]
        public int Round
        {
            get { return Results.Count; }
        }

        [BsonIgnore]
        public UserStatus? AllUserStatus
        {
            get { return CheckAllUserStatus(); }
            set {
                _users = _users.Select(u =>
                {
                    if (value != null) u.Status = (UserStatus)value;
                    return u;
                }).ToList();
            }
        }

        protected UserStatus? CheckAllUserStatus()
        {
            string status = _users.Aggregate("", (acc, user) =>
            {
                return acc.Equals("") || acc.Equals(user.Status.ToString()) ? user.Status.ToString() : "null";
            });
            return status.Equals("") || status.Equals("null") ? null : Enum.Parse<UserStatus>(status);
        }

        public void DefineUsersStatus(UserStatus driverStatus, UserStatus othersStatus)
        {
            AllUserStatus = othersStatus;
            var driverIndex = Users.FindIndex(u => u.Id.Equals(Game?.DriverId));
            Users[driverIndex].Status = driverStatus;
        }

        public string GetNextDriverId()
        {
            if (Game == null || Config.ChangeDriver == false) return Users.First().Id;
            var nextIndex = Users.FindIndex(u => u.Id.Equals(Game.DriverId)) + 1;
            return Users.Count - 1 >= nextIndex ? Users[nextIndex].Id : Users.First().Id;
        }

        public void AddResult(Game? game)
        {
            if (game != null) Results.Add(game);
        }

    }
}
