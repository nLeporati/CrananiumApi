﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace CrananiumApi.Domain.Model
{
    //public enum Scene
    //{
    //    [EnumMember(Value = SceneConst.Home)]
    //    Home,
    //    [EnumMember(Value = SceneConst.Lobby)]
    //    Lobby,
    //    [EnumMember(Value = SceneConst.Next)]
    //    Next,
    //    [EnumMember(Value = SceneConst.GameInfo)]
    //    GameInfo,
    //    [EnumMember(Value = SceneConst.GameStart)]
    //    GameStart,
    //    [EnumMember(Value = SceneConst.GameRun)]
    //    GameRun,
    //    [EnumMember(Value = SceneConst.GameEnd)]
    //    GameEnd,
    //    [EnumMember(Value = SceneConst.Results)]
    //    Results
    //}

    public static class Scene
    {
        public const string Home = "home";
        public const string Lobby = "lobby";
        public const string Next = "next";
        public const string GameInfo = "game:info";
        public const string GameStart = "game:start";
        public const string GameRun = "game:run";
        public const string GameEnd = "game:end";
        public const string Results = "results";
    }

    //public class Scene
    //{
    //    private Scene(string value) { Value = value; }

    //    public string Value { get; private set; }

    //    public static Scene Home { get { return new Scene(SceneEnum.Home); } }
    //    public static Scene Lobby { get { return new Scene(SceneEnum.Lobby); } }
    //    public static Scene Next { get { return new Scene(SceneEnum.Next); } }
    //    public static Scene GameInfo { get { return new Scene(SceneEnum.GameInfo); } }
    //    public static Scene GameStart { get { return new Scene(SceneEnum.GameStart); } }
    //    public static Scene GameRun { get { return new Scene(SceneEnum.GameRun); } }
    //    public static Scene GameEnd { get { return new Scene(SceneEnum.GameEnd); } }
    //    public static Scene Results { get { return new Scene(SceneEnum.Results); } }

    //    public override string ToString()
    //    {
    //        return Value;
    //    }
    //}
}
