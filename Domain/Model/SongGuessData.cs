﻿namespace CrananiumApi.Domain.Model
{
    public class SongGuessData : GameData
    {
        public object? Song { get; set; }

        public SongGuessData() { }
    }

    public class Song
    {
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Audio { get; set; }
        public string Image { get; set; }

        public Song(string name, string artist, string audio, string image)
        {
            Name = name;
            Artist = artist;
            Audio = audio;
            Image = image;
        }
    }
}
