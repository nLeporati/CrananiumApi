﻿namespace CrananiumApi.Domain.Model
{
    public class Message
    {
        public string RoomId { get; set; } = null!;
        public string UserId { get; set; } = null!;
        public string Text { get; set; } = null!;
        public bool Guess { get; set; }
    }
}
