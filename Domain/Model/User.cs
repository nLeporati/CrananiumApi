﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.Text.Json.Serialization;

namespace CrananiumApi.Domain.Model
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum UserStatus
    {
        idle,
        ready,
        playing,
        loading,
        created,
        offline,
        waiting
    }

    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = null!;
        public string Name { get; set; }
        [BsonRepresentation(BsonType.String)]
        public UserStatus Status { get; set; }

        public User(string name)
        {
            Name = name;
            Status = UserStatus.idle;
        }
    }
}
