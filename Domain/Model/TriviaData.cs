﻿namespace CrananiumApi.Domain.Model
{
    public class TriviaData : GameData
    {
        public object? Question { get; set; }

        public TriviaData() { }
    }
}
