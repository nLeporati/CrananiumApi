﻿using MongoDB.Bson.Serialization.Attributes;
using System.Text.Json.Serialization;

namespace CrananiumApi.Domain.Model
{
    //[BsonKnownTypes(typeof(SongGuessGame))]
    //[JsonDerivedType(typeof(SongGuessGame))]
    public class Game
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string Id { get; set; } = null!;
        public string DriverId { get; set; }
        public string Mode { get; set; }
        public GameProps Props { get; set; }
        public GameData? Data { get; set; }
        public List<string> UsersAnswered { get; set; }

        public Game(GameProps props, string mode, string driverId)
        {
            Props = props;
            Mode = mode;
            DriverId = driverId;
            UsersAnswered = new List<string>() { };
        }
    }

    //[BsonKnownTypes(typeof(SongGuess))]
    public class Game<T> : Game where T : GameData
    {
        new public T Data { get; set; }
        public Game(GameProps props, string mode, string driverId, T data) : base(props, mode, driverId)
        {
            Data = data;
        }
    }

    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(typeof(SongGuessData))]
    [BsonKnownTypes(typeof(TriviaData))]
    [JsonDerivedType(typeof(SongGuessData))]
    [JsonDerivedType(typeof(TriviaData))]
    public class GameData { }

    public class GameProps
    {
        public string Name { get; set; } = null!;
        public string Variant { get; set; } = null!;
        public string Style { get; set; } = null!;
        public string Category { get; set; } = null!;
    }

    public class GameConfig
    {
        public int Rounds { get; set; }
        public bool ChangeDriver { get; set; }
        public List<string> Games { get; set; }

        public GameConfig(int rounds, bool changeDriver, List<string> games)
        {
            Rounds = rounds;
            ChangeDriver = changeDriver;
            Games = games;
        }
    }
}
