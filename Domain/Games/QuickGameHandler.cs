﻿using CrananiumApi.Data.Config.Games;
using CrananiumApi.Domain.Datasource;
using CrananiumApi.Domain.Model;
using CrananiumApi.Domain.Service;
using static CrananiumApi.Domain.Games.QuickGameHandler;

namespace CrananiumApi.Domain.Games
{
    public class QuickGameHandler : IGameHandler
    {
        protected readonly RoomDatasource _roomData;
        protected readonly GameDatasource _gameData;
        protected readonly GameService _gameService;

        protected Room Room { get; set; } = null!;
        public virtual GameData GameData { get; set; } = null!;

        public QuickGameHandler(RoomDatasource roomDatasource, GameDatasource gameDatasource, GameService gameService)
        {
            _roomData = roomDatasource;
            _gameData = gameDatasource;
            _gameService = gameService;
        }

        public virtual HashSet<Type> Support => new() { };

        protected string GameName => Room!.Game!.Props.Name ?? "";
        protected bool HasData => GameData.GetType()
            .GetProperties()
            .Select(prop => prop.GetValue(GameData))
            .Any(value => value != null);


        public virtual async Task Execute(Room room, GameData data)
        {
            Room = room;
            GameData = data;

            // TODO make another class to handler every scene
            switch (room.Scene)
            {
                case Scene.Lobby:
                    await OnLobby(room);
                    break;
                case Scene.GameStart:
                    await OnGameStart(room);
                    break;
                case Scene.GameRun:
                    await OnGameRun(room);
                    break;
                case Scene.GameEnd:
                    await OnGameEnd(room);
                    break;
                case Scene.Next:
                    await OnGameNext(room);
                    break;
                case Scene.Results:
                    await OnResults(room);
                    break;
                default:
                    break;
            }
        }

        protected virtual async Task OnLobby(Room room)
        {
            if (room.AllUserStatus.Equals(UserStatus.ready))
            {
                room.Scene = Scene.Next;
                room.AllUserStatus = UserStatus.idle;
                await _roomData.UpdateAsync(room);
            }
        }

        protected virtual async Task OnGameNext(Room room)
        {
            // TODO is necesary an endpoint to handle next/results logic
            //if (room.Results.Count >= room.Config.Rounds)
            //{
            //    room.Scene = Scene.Results;
            //    room.AllUserStatus = UserStatus.idle;
            //    //room.AddResult(room.Game);
            //    //room.Game = null;
            //    await _roomData.UpdateAsync(room);
            //    return;
            //}
            await Task.Delay(1000);

            if (HasData)
                room.Game = await CreateNextGame(room);            

            room.Scene = Scene.GameInfo;
            room.DefineUsersStatus(UserStatus.idle, UserStatus.ready);

            await _roomData.UpdateAsync(room);
        }

        protected virtual async Task OnGameStart(Room room)
        {
            if (HasData)
            {
                room.Scene = Scene.GameRun;
                room.AllUserStatus = UserStatus.waiting;
                await _roomData.UpdateAsync(room);
            }
        }

        protected virtual async Task OnGameRun(Room room)
        {
            await Task.Delay(0);
        }

        // WARNING this.GameData & this.Room not defined
        public virtual async Task OnGuess(Room room, string userId)
        {
            if (room.Game != null)
            {
                room.Scene = Scene.GameEnd;
                //room.AddResult(room.Game);
                room.Game.UsersAnswered.Add(userId);
                room.DefineUsersStatus(UserStatus.idle, UserStatus.ready);
                await _roomData.UpdateAsync(room);
            }
        }

        protected virtual async Task OnGameEnd(Room room)
        {
            // TODO add here the result (and not the new game)
            await Task.Delay(0);
        }

        protected virtual async Task OnResults(Room room)
        {
            if (room.Game != null && room.Results.Count < room.Config.Rounds)
            {
                await Task.Delay(1000);
                room.AddResult(room.Game);
                room.Game = null;
                await _roomData.UpdateAsync(room);
            }
        }

        protected virtual async Task<Game> CreateNextGame(Room room)
        {
            room.AddResult(room.Game); // remove from here
            var newGame = await _gameService.GenerateNextGame(room);
            var updatedGame = await _gameData.UpdateAsync(newGame.Id, newGame);
            return updatedGame;
        }
    }
}
