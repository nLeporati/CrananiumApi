﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Games
{
    public class RoomHandler
    {
        private readonly GameFactory _gameFactory;
        private readonly IEnumerable<IGameHandler> _handlers;
        //private readonly IHubContext<MessagesHub> _hubContext;

        public RoomHandler(GameFactory gameFactory, IEnumerable<IGameHandler> gameHandlers)
        {
            _gameFactory = gameFactory;
            _handlers = gameHandlers;
        }

        private IGameHandler GetGameHandler(GameProps props)
        {
            // TODO change the logic to work with QuickGame not specific games
            var key = $"{props.Name}:{props.Variant}";
            var gameType = _gameFactory.From(key).GetType();
            var handler = _handlers.Where(h => h.Support.Contains(gameType)).FirstOrDefault()
                ?? _handlers.Where(h => h.Support.Count == 0).FirstOrDefault();
            return handler ?? throw new HttpRequestException($"No handler configured for {key}");
        }   

        public async Task HandleRoom(Room room)
        {
            var game = room.Game ?? throw new HttpRequestException("Not active game found");
            if (game.Data == null) throw new HttpRequestException("Not game data found");
            await GetGameHandler(game.Props).Execute(room, game.Data);
        }

        public async Task HandleMessage(Room room, Message message)
        {
            var game = room.Game ?? throw new HttpRequestException("No active game found");

            await GetGameHandler(game.Props).OnGuess(room, message.UserId);
        }
    }
}
