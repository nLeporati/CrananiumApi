﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Games
{
    public interface IGameHandler
    {
        HashSet<Type> Support { get; }
        Task Execute(Room room, GameData data);
        Task OnGuess(Room room, string userId);
    }
}
