﻿using CrananiumApi.Data.Config.Games;
using CrananiumApi.Domain.Datasource;
using CrananiumApi.Domain.Model;
using CrananiumApi.Domain.Service;

namespace CrananiumApi.Domain.Games
{
    public class SongGuessGameHandler : QuickGameHandler
    {
        public new SongGuessData? GameData { get; set; } = null!;

        public SongGuessGameHandler(RoomDatasource roomDatasource, GameDatasource gameDatasource, GameService gameService)
            : base(roomDatasource, gameDatasource, gameService)
        { }

        override public HashSet<Type> Support => new()
        {
            typeof(SongGuessChosen),
            typeof(SongGuessRandom)
        };       
    }
}
