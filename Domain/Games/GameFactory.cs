﻿using CrananiumApi.Data.Config.Games;
using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Games
{
    public class GameFactory
    {
        private readonly IEnumerable<IGameSettings> _games;

        public GameFactory(IEnumerable<IGameSettings> games)
        {
            _games = games;
        }

        public IGameSettings From(string key)
        {
            var game = _games.Where(g => g.Key.Equals(key)).FirstOrDefault()
                ?? throw new HttpRequestException($"Game configuration not found for key \'{key}\'");
            return game;
        }
    }
}
