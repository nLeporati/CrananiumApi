﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Datasource
{
    public interface UserDatasource
    {
        Task<User> CreateAsync(string username);
        Task<User> UpdateAsync(User user);
    }
}
