﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Datasource
{
    public interface RoomDatasource
    {
        Task<Room> GetAsync(string roomId);
        //Task<Room> FindAsync<T>(string roomId) where T : Game;
        //Task<Room> CreateAsync(User user);
        Task<Room> CreateAsync(Room room);
        //Task<Room> CreateAsync<T>(User user) where T : Game;
        //Task<Room> UpdateAsync(Room room);
        Task<Room> UpdateAsync(Room room);
        Task<Room> AddUserAsync(string roomId, User user);
        Task AddGameAsync(string roomId, Game game);
        Task<Room> UpdateUserAsync(string roomId, User user);
        Task UpdateUsersStatusAsync(string roomId, UserStatus status);
        Task<Room> UpdateSceneAsync(string roomId, string scene);
        Task<Room> SetGameDataAsync(string roomId, GameData data);
    }
}
