﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Datasource
{
    public interface GameDatasource
    {
        Task<T> GetAsync<T>(string id) where T : Game;
        Task<T> CreateAsync<T>(T game) where T : Game;
        Task<T> UpdateAsync<T>(string id, T game) where T : Game;
        Task<T> SetDataAsync<T>(string id, GameData data) where T : Game;
    }
}
