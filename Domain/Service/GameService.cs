﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Service
{
    public interface GameService
    {
        public Task<Game> GenerateNextGame(Room room);
    }
}
