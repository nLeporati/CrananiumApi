﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Service
{
    public interface UserService
    {
        Task<User> CreateUser(string username);
        Task<User> UpdateUser(User user);
    }
}
