﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Domain.Service
{
    public interface RoomService
    {
        Task<Room> GetRoom(string roomId);
        Task<Room> CreateRoom(string userId, GameConfig? config);
        Task<Room> UpdateRoom(Room room);
        Task<Room> JoinRoom(string roomId, string username);
        Task<Room> AddGame(Room room, Game game);
        Task<Room> UpdateUser(string roomId, User user);
        Task<Room> UpdateRoomScene(string roomId, string scene);
        Task UpdateGame(string roomId, Game game);
        Task UpdateGameData(string roomId, GameData data);
    }
}
