# CrananiumApi

## To do

Add driver prop on the game (change the driver for every game)

Add GameConfig prop (handle the game list and rounds)

Handle the last game (goto result) in the changes handler

## Clean the code

Maybe use less classes for entites, and only create hineritance

Clean REST and WS services

Define DB schemas

## Future tech improvements

Monorepo

Minimal API

Add UX and design

Event driven Arch

## Deploy

### Google Cloud Run
���
gcloud run deploy crananium-api --allow-unauthenticated --region us-west2 --source .
���
