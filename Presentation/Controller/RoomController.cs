﻿using CrananiumApi.Data.Config;
using CrananiumApi.Domain.Games;
using CrananiumApi.Domain.Model;
using CrananiumApi.Domain.Service;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Dynamic;

namespace CrananiumApi.Presentation.Controller
{
    [ApiController]
    [Route("rooms")]
    public class RoomController : ControllerBase
    {
        private readonly RoomHandler _roomHandler;
        private readonly RoomService _roomService;
        private readonly GameService _gameService;

        public RoomController(RoomHandler roomHandler, RoomService service, GameService gameService)
        {
            _roomHandler = roomHandler;
            _roomService = service;
            _gameService = gameService;
        }

        [HttpPost]
        public async Task<ActionResult<Room>> CreateRoom(string username, [FromBody] GameConfig? config)
        {
            var room = await _roomService.CreateRoom(username, config);
            return Ok(room);
        }

        [HttpPost]
        [Route("update")]
        public async Task<ActionResult<Room>> UpdateRoom(Room room)
        {
            var entity = await _roomService.UpdateRoom(room);
            return Ok(entity);
        }

        [HttpGet]
        [Route("{roomId}")]
        public async Task<ActionResult<Room>> GetRoom(string roomId)
        {
            var room = await _roomService.GetRoom(roomId);
            return Ok(room);
        }

        [HttpPost]
        [Route("{roomId}/users/{username}")]
        public async Task<ActionResult<Room>> JoinRoom([FromRoute] string roomId, string username)
        {
            var room = await _roomService.JoinRoom(roomId, username);
            return Ok(room);
        }

        [HttpPut]
        [Route("{roomId}/users")]
        public async Task<ActionResult<Room>> UpdateUserStatus(string roomId, [FromBody] User user)
        {
            var room = await _roomService.UpdateUser(roomId, user);
            return Ok(room);
        }

        [HttpPost] //TODO Put
        [Route("{roomId}/games/{gameId}")]
        public async Task<ActionResult> UpdateGame(string roomId, string gameId, [FromBody] dynamic jsonData)
        {
            // TODO convert diferent objects
            var converter = new ExpandoObjectConverter();
            var exObjExpandoObject = JsonConvert.DeserializeObject<ExpandoObject>(jsonData.ToString(), converter);

            try
            {
                var song = exObjExpandoObject.song;
                var data = new SongGuessData() { Song = song };
                await _roomService.UpdateGameData(roomId, data);
            }
            catch { }
            try
            {
                var question = exObjExpandoObject.question;
                var data = new TriviaData() { Question = question };
                await _roomService.UpdateGameData(roomId, data);
            }
            catch { }
            //var game = await _gameService.UpdateGameData(gameId, data);
            //await _roomService.UpdateGame(roomId, game);
            //await _roomService.UpdateGameData(roomId, data);
            return Ok();
        }

        [HttpPost]
        [Route("{roomId}/{scene}")]
        public async Task<ActionResult<Room>> UpdateScene(string roomId, string scene)
        {
            var room = await _roomService.UpdateRoomScene(roomId, scene);
            return Ok(room);
        }

        [HttpPost]
        [Route("{roomId}/{scene}/handler")]
        public async Task<ActionResult<Room>> UpdateSceneHandlerTest(string roomId, string scene)
        {
            var room = await _roomService.UpdateRoomScene(roomId, scene);
            await _roomHandler.HandleRoom(room);
            return Ok(room);
        }

        public static bool HasProperty(dynamic obj, string name)
        {
            try
            {
                var t = obj[name];
                return t != null;
            } catch
            {
                return false;
            }
        }
    }
}
