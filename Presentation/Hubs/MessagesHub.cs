﻿using CrananiumApi.Domain.Games;
using CrananiumApi.Domain.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using CrananiumApi.Domain.Model;

namespace CrananiumApi.Presentation.Hubs
{
    public class MessagesHub : Hub
    {
        public async Task Watch(string roomId)
        {
            var groupName = $"room:{roomId}";
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        public async Task Messages(Message message, [FromServices] RoomService roomService, [FromServices] RoomHandler roomHandler)
        {
            var groupName = $"room:{message.RoomId}";
            var group = Clients.OthersInGroup(groupName);
            await group.SendAsync("new-message", message);

            if (message.Guess)
            {            
                try
                {
                    var room = await roomService.GetRoom(message.RoomId);
                    if (room != null) await roomHandler.HandleMessage(room, message);
                }
                catch (Exception ex)
                {
                    await Clients.Caller.SendAsync("error", ex.Message);
                }
            }
        }
    }
}
