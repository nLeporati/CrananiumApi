﻿using CrananiumApi.Domain.Games;
using CrananiumApi.Domain.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Runtime.CompilerServices;
using MongoDB.Driver;
using CrananiumApi.Data.MongoRepository.Config;

namespace CrananiumApi.Presentation.Hubs
{
    public class RoomHub : Hub
    {
        public async Task Room(string roomId, [FromServices] MongoDatabase db, [FromServices] RoomHandler roomHandler)
        {
            var groupName = $"room:{roomId}";
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            var options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup };

            using var cursor = db.Rooms.Watch(ConfigurePipeline(roomId), options);
            await cursor.ForEachAsync(async change =>
            {
                var room = change.FullDocument;
                await Clients.Caller.SendAsync("room-changes", room);
                
                try { await roomHandler.HandleRoom(room); }
                catch (Exception ex) { await Clients.Caller.SendAsync("error", ex.Message); }
            });
        }

        public async IAsyncEnumerable<Room> WatchRoom(string roomId, [FromServices] MongoDatabase db, [FromServices] RoomHandler roomHandler, [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            var options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup };

            using (var cursor = db.Rooms.Watch(ConfigurePipeline(roomId), options))
            {
                cancellationToken.ThrowIfCancellationRequested();
                foreach (var change in cursor.ToEnumerable())
                {
                    var room = change.FullDocument;
                    await roomHandler.HandleRoom(room);
                    yield return room;
                }
            }
        }

        private PipelineDefinition<ChangeStreamDocument<Room>, ChangeStreamDocument<Room>> ConfigurePipeline(string roomId)
        {
            List<IPipelineStageDefinition> pipeline = new List<IPipelineStageDefinition>();
            pipeline.Add(PipelineStageDefinitionBuilder.Match(ConfigureFilters(roomId)));
            return pipeline;
        }

        private FilterDefinition<ChangeStreamDocument<Room>> ConfigureFilters(string roomId)
        {
            var builder = Builders<ChangeStreamDocument<Room>>.Filter;
            var filters = builder.Eq(x => x.FullDocument.Id, roomId);
            return filters;
        }
    }
}
