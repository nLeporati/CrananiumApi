﻿using CrananiumApi.Data.MongoRepository.Config;
using CrananiumApi.Domain.Datasource;
using CrananiumApi.Domain.Model;
using MongoDB.Driver;

namespace CrananiumApi.Data.Repository
{
    public class UserMongoRepository : UserDatasource
    {
        private readonly MongoDatabase _db;

        public UserMongoRepository(MongoDatabase db) { _db = db; }

        public async Task<User> GetAsync(string id)
        {
            return await _db.Users.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<User> CreateAsync(string username)
        {
            var entity = new User(username);
            await _db.Users.InsertOneAsync(entity);
            return await GetAsync(entity.Id);
        }

        public async Task<User> UpdateAsync(User user)
        {
            await _db.Users.ReplaceOneAsync(x => x.Id == user.Id, user);
            return await GetAsync(user.Id);
        }
    }
}
