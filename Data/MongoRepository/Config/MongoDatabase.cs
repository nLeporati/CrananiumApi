﻿using CrananiumApi.Domain.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace CrananiumApi.Data.MongoRepository.Config
{
    public static class MongoExtensions
    {
        public static IMongoCollection<T> GetCollection<T>(this IMongoDatabase db)
        {
            var name = typeof(T).Name.ToLower().Replace("entity", "") + "s";
            return db.GetCollection<T>(name);
        }
    }
    public class MongoDatabase
    {
        public readonly IMongoDatabase _db;

        public MongoDatabase(IOptions<DatabaseConfig> config)
        {
            var mongoCli = new MongoClient(config.Value.ConnectionString);
            var mongoDb = mongoCli.GetDatabase(config.Value.DatabaseName);
            _db = mongoDb;
        }

        public IMongoCollection<T> GetCollection<T>() => _db.GetCollection<T>();

        public IMongoCollection<Room> Rooms => _db.GetCollection<Room>();

        public IMongoCollection<User> Users => _db.GetCollection<User>();

        public IMongoCollection<Game> Games => _db.GetCollection<Game>();
    }
}
