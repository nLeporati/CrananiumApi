﻿namespace CrananiumApi.Data.MongoRepository.Config
{
    public class DatabaseConfig
    {
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;

        public Collections CollectionsNames { get; set; } = null!;

        public class Collections
        {
            public string Rooms { get; set; } = null!;
            public string Users { get; set; } = null!;
            public string Games { get; set; } = null!;
        }
    }
}
