﻿using CrananiumApi.Data.MongoRepository.Config;
using CrananiumApi.Domain.Datasource;
using CrananiumApi.Domain.Model;
using MongoDB.Driver;

namespace CrananiumApi.Data.Repository
{
    public class RoomMongoRepository : RoomDatasource
    {
        private MongoDatabase _db;

        public RoomMongoRepository(MongoDatabase db)
        {
            _db = db;
        }

        public async Task<Room> GetAsync(string id)
        {
            var entity = await _db.Rooms.Find(x => x.Id.Equals(id)).FirstOrDefaultAsync();
            if (entity == null) throw new HttpRequestException("Room not found");
            return entity;
        }

        public async Task<Room> CreateAsync(Room room)
        {
            await _db.Rooms.InsertOneAsync(room);
            return await GetAsync(room.Id);
        }

        public async Task<Room> UpdateAsync(Room room)
        {
            var filter = Builders<Room>.Filter.Eq(x => x.Id, room.Id);
            await _db.Rooms.ReplaceOneAsync(filter, room, new ReplaceOptions { IsUpsert = true });
            return await GetAsync(room.Id);
        }

        public async Task<Room> AddUserAsync(string roomId, User user)
        {
            var update = Builders<Room>.Update.Push("users", user);
            await _db.Rooms.UpdateOneAsync(x => x.Id == roomId, update);
            return await GetAsync(roomId);
        }

        public async Task<Room> UpdateUserAsync(string roomId, User user)
        {
            var filter = Builders<Room>.Filter.Where(x => x.Id == roomId && x.Users.Any(u => u.Id == user.Id));
            //var filter = Builders<Room>.Filter.Eq(x => x.Id, roomId)
            //    & Builders<Room>.Filter.ElemMatch(x => x.Users, Builders<User>.Filter.Eq(x => x.Id, user.Id));

            var update = Builders<Room>.Update.Set(x => x.Users[-1], user);

            await _db.Rooms.UpdateOneAsync(filter, update);
            return await GetAsync(roomId);
        }

        public async Task UpdateUsersStatusAsync(string roomId, UserStatus status)
        {
            var filter = Builders<Room>.Filter.Eq(x => x.Id, roomId);
            var update = Builders<Room>.Update.Set("users.$[].status", status);

            await _db.Rooms.UpdateOneAsync(filter, update);
        }

        public async Task<Room> UpdateSceneAsync(string roomId, string scene)
        {
            var update = Builders<Room>.Update.Set("scene", scene);
            await _db.Rooms.UpdateOneAsync(x => x.Id == roomId, update);
            return await GetAsync(roomId);
        }

        public async Task AddGameAsync(string roomId, Game game)
        {
            var update = Builders<Room>.Update.Set("game", game);
            await _db.Rooms.UpdateOneAsync(x => x.Id == roomId, update);
        }

        public async Task<Room> SetGameDataAsync(string roomId, GameData data)
        {
            var update = Builders<Room>.Update.Set("game.data", data);
            await _db.Rooms.UpdateOneAsync(x => x.Id == roomId, update);
            return await GetAsync(roomId);
        }
    }
}
