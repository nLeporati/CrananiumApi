﻿using CrananiumApi.Data.MongoRepository.Config;
using CrananiumApi.Domain.Datasource;
using CrananiumApi.Domain.Model;
using MongoDB.Driver;

namespace CrananiumApi.Data.Repository
{
    public class GameMongoRepository : GameDatasource
    {
        private readonly IMongoDatabase _db;

        public GameMongoRepository(MongoDatabase db)
        {            
            _db = db._db;
        }

        public async Task<T> GetAsync<T>(string id) where T : Game
        {
            var collection = _db.GetCollection<T>();
            return await collection.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<T> CreateAsync<T>(T game) where T : Game
        {
            var collection = _db.GetCollection<T>();
            await collection.InsertOneAsync(game);
            return game;
        }

        public async Task<T> UpdateAsync<T>(string id, T game) where T : Game
        {
            var collection = _db.GetCollection<T>();
            var filter = Builders<T>.Filter.Eq(x => x.Id, game.Id);
            await collection.ReplaceOneAsync(filter, game, new ReplaceOptions { IsUpsert = true });
            return await GetAsync<T>(id);
        }


        public async Task<T> SetDataAsync<T>(string id, GameData data) where T : Game
        {
            var update = Builders<T>.Update.Set("data", data);
            var collection = _db.GetCollection<T>();
            await collection.UpdateOneAsync(x => x.Id == id, update);
            return await GetAsync<T>(id);
        }
    }
}
