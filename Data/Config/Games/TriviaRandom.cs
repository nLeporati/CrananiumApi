﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Data.Config.Games
{
    public class TriviaRandom : IGameSettings
    {
        public string Key => Const.Keys.TRIVIA_RANDOM;
        public GameProps Props => new()
        {
            Name = Const.Games.TRIVIA,
            Variant = Const.Variants.RANDOM,
            Style = Const.Styles.SOLO,
            Category = Const.Categories.SLEUTH_SOLVE,
        };

        public GameData Data => new TriviaData();
    }
}
