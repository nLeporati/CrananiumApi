﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Data.Config.Games
{
    public class SongGuessChosen : IGameSettings
    {
        public string Key => Const.Keys.SONGGUESS_CHOSEN;
        public GameProps Props => new()
        {
            Name = Const.Games.SONGGUESS,
            Variant = Const.Variants.CHOSEN,
            Style = Const.Styles.SOLO,
            Category = Const.Categories.ACT_HUM,
        };

        public GameData Data => new SongGuessData();
    }
}
