﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Data.Config.Games
{
    public interface IGameSettings
    {
        string Key { get; }
        GameProps Props { get; }
        GameData Data { get; }
    }
}
