﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Data.Config.Games
{
    public class SongGuessRandom : IGameSettings
    {
        public string Key => Const.Keys.SONGGUESS_RANDOM;
        public GameProps Props => new()
        {
            Name = Const.Games.SONGGUESS,
            Variant = Const.Variants.RANDOM,
            Style = Const.Styles.COOP,
            Category = Const.Categories.ACT_HUM,
        };

        public GameData Data => new SongGuessData();
    }
}
