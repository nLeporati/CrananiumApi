﻿using CrananiumApi.Domain.Model;

namespace CrananiumApi.Data.Config
{
    public class AppConfig
    {
        public int DefaultRounds { get; set; } = 5;
        public bool DefaultChangeDriver { get; set; } = true;
        public HashSet<string> ConfiguredGames { get; set; } = new()
        {
            Const.Keys.SONGGUESS_CHOSEN,
            Const.Keys.SONGGUESS_RANDOM,
            Const.Keys.TRIVIA_RANDOM
        };

        public GameConfig DefaultGameConfig => new GameConfig(
            DefaultRounds,
            DefaultChangeDriver,
            ConfiguredGames.ToList()
        );
    }
}
