﻿using System.Text.Json;

namespace CrananiumApi.Data.Config
{
    public static class Const
    {
        public static class Keys
        {
            public const string SONGGUESS_CHOSEN = "songguess:chosen";
            public const string SONGGUESS_RANDOM = "songguess:random";
            public const string TRIVIA_RANDOM = "trivia:random";
        }
        public static class Games
        {
            public const string SONGGUESS = "songguess";
            public const string TRIVIA = "trivia";
        }

        public static class Categories
        {
            public const string ACT_HUM = "act-hum";
            public const string WORD_PLAY = "word-play";
            public const string SLEUTH_SOLVE = "sleuth-solve";
            public const string SKETCH_SCULPT = "sketch-sculpt";
        }

        public static class Styles
        {
            public const string COOP = "coop-style";
            public const string SOLO = "solo-style";
        }

        public static class Modes
        {
            public const string ALL = "all-plays";
            public const string TEAM = "team-plays";
        }

        public static class Variants
        {
            public const string CHOSEN = "chosen";
            public const string RANDOM = "random";
        }

        public static T? Map<F, T>(F from)
        {
            var json = JsonSerializer.Serialize(from);
            var to = JsonSerializer.Deserialize<T>(json);

            return to;
        }
    }
}
